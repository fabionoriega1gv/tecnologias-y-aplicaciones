document.getElementById('buscarButton').addEventListener('click', function() {
    const userId = document.getElementById('userId').value;
    axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .then(function (response) {
            const user = response.data;
            mostrarInformacionUsuario(user);
        })
        .catch(function (error) {
            console.error('Error en la solicitud:', error);
            mostrarError();
        });
});

function mostrarInformacionUsuario(user) {
    // Mostrar la información en el documento
    document.getElementById('nombre').innerText = user.name;
    document.getElementById('nombreUsuario').innerText = user.username;
    document.getElementById('email').innerText = user.email;

    const domicilio = `${user.address.street} ${user.address.suite}, ${user.address.city}`;
    document.getElementById('domicilio').innerText = domicilio;
}
function mostrarError() {
    document.getElementById('userInfo').innerHTML = '<p>No se encontró información</p>';
}

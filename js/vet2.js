// Fetch cargar raza
document.getElementById('cargarRaza').addEventListener('click', () => {
  // Fetch lista de raza
  fetch('https://dog.ceo/api/breeds/list')
    .then(response => response.json())
    .then(data => {
      const razacombo = document.getElementById('razacombo');
      data.message.forEach(breed => {
        const option = document.createElement('option');
        option.value = breed;
        option.textContent = breed;
        razacombo.appendChild(option);
      });
    })
    .catch(error => console.error('Error fetching breeds:', error));
});

// Fetch de imagenes random al "Cargar imagen"
document.getElementById('cargarImagen').addEventListener('click', () => {
  const razacombo = document.getElementById('razacombo');
  const selectedBreed = razacombo.value;

  fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
    .then(response => response.json())
    .then(data => {
      const dogImage = document.getElementById('dogImage');
      dogImage.src = data.message;
    })
    .catch(error => console.error('Error fetching image:', error));
});